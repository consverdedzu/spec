# Standard meta data

## Preview

- [Search result](/res/adblockplus.org/screenshots/standard-meta-data.png)

## Contents

| Page attribute | Required | Default |
| :-- | :-- | :-- |
| `title` | Yes | None |
| `description` | No | None |
